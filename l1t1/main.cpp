#include <iostream>
using namespace std;

int main()
{
    float inp;
    cout << "Enter temperature in C: ";
    cin >> inp;

    int select;
    do
    {
        cout << "To which unit transform?\n1) Fahrenheit\n2) Kelvin\n3) Cancel..." << endl;
        cin >> select;
        if ( select < 1 || select > 3 )
            cout << "Wrong selection! ..." << endl << endl;
    }
    while ( select != 1 & select != 2 & select != 3 );

    if ( select == 1 ) {
        float out = inp * 9 / 5 + 32;
        cout << inp << " °C = " << out << " °F" << endl;
    }
    else
    if ( select == 2 ) {
        float out = inp + 273.15;
        cout << inp << " °C = " << out << " K" << endl;
    }
    else
        cout << "canceled" << endl;
}