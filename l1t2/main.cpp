#include <iostream>
#include <cmath>
using namespace std;

struct Roots
{
	double x1,x2;
	int n; //0,1,2

	void resolve(double a, double b, double c)
	{
		double discr = pow(b, 2) - 4 * a * c;
		if ( discr > 0 )
		{
			n = 2;
			x1 = ( -b + sqrt(discr) ) / ( 2 * a );
        	x2 = ( -b - sqrt(discr) ) / ( 2 * a );
			cout << "Roots are real and different." << endl;
        	cout << "x1 = " << x1 << endl;
        	cout << "x2 = " << x2 << endl;
		}
		else
		if ( discr == 0 )
		{
			n = 1;
			x1 = -b / ( 2 * a );
			cout << "Roots are real and same." << endl;
			cout << "x = " << x1 << endl;
		}
		else
		{
			n = 0;
			cout << "Roots are complex."  << endl;
		}

	}
};

struct SquareEq
{
	double a,b,c;
	Roots sol;
};

int main()
{
	SquareEq quad;
	cout << "Enter a, b, c values:" << endl;
	cin >> quad.a;
	cin >> quad.b;
	cin >> quad.c;
	quad.sol.resolve(quad.a,quad.b,quad.c);
}