#include <iostream>
#include <Windows.h>
#include "AllegroBase.hpp"

using namespace std;

const int FPS = 60;
const int SCREEN_W = 640;
const int SCREEN_H = 480;

class Point
{
    protected:
        double x_;
        double y_;
        double dx_;
        double dy_;

    public:
        Point() : 
            x_ ( 0 ),
            y_ ( 0 ),
            dx_ ( 0 ),
            dy_ ( 0 )
        {
        }

        virtual void Reset()
        {
            x_ = ( rand() % ( SCREEN_W - 100 ) ) + 50;
            y_ = ( rand() % ( SCREEN_H - 100 ) ) + 50;
            dx_ = 5.0 - rand() % 11;
            dy_ = 5.0 - rand() % 11;
        }

        virtual void Draw() = 0;

        virtual void Move()
        {
            x_ += dx_;
            y_ += dy_;
            if ( ( x_ < 0.0) || ( x_ > SCREEN_W ) )
            {
                dx_ = -dx_;
                x_ += dx_;
            }
            if ( ( y_ < 0.0) || ( y_ > SCREEN_H ) )
            {
                dy_ = -dy_;
                y_ += dy_;
            }
        }
};

class Square : public Point
{
    protected:
        double side_;
    
    public:
        Square ( double side ) :
            Point(),
            side_ ( side )
        {
        }

        virtual void Draw()
        {
            double half = side_ / 2;
            al_draw_filled_rectangle ( x_ - half, y_ - half, x_ + half, y_ + half, al_map_rgb( 255, 0, 0 ) );
        }

        virtual void Move()
        {
            x_ += dx_;
            y_ += dy_;

            double half = side_ / 2;
            if ( ( x_ - half < 0.0) || ( x_ + half > SCREEN_W ) )
            {
                dx_ = -dx_;
                x_ += dx_;
            }
            if ( ( y_ - half < 0.0) || ( y_ + half > SCREEN_H ) )
            {
                dy_ = -dy_;
                y_ += dy_;
            }
        }
};

class AllegroApp : public AllegroBase
{
    private:
        Square Square_;

    public:
        AllegroApp() :
            AllegroBase(),
            Square_( 10.0 + rand() % 20 )
        {
            Square_.Reset(); //sets over 0 value new random values
        }

        virtual void Fps()
        {
            Square_.Move();
        }

        virtual void Draw()
        {
            al_clear_to_color( al_map_rgb( 0, 0, 0 ) );
            Square_.Draw();
        }

        virtual void OnKeyDown( const ALLEGRO_KEYBOARD_EVENT &keyboard )
        {
            switch ( keyboard.keycode )
            {
                case ALLEGRO_KEY_ENTER:
                    Square_.Reset();
                    break;
                case ALLEGRO_KEY_ESCAPE:
                    Exit();
                    break;
            }

        }
};


int main(int argc, char **argv)
{
    srand( time(0) );

    AllegroApp app;
    if ( !app.Init( SCREEN_W, SCREEN_H, FPS ) )
    {
        return 1;
    }
    app.Run();

    return 0;
}