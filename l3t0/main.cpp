// Проверка иерархии
#include <iostream>

using namespace std;

class Parent
{
public:
    void M1()
    {
        cout << "Parent::M1" << endl;
    }
    virtual void M2()
    {
        cout << "Parent::M2" << endl;
    }
    void M3()
    {
        cout << "Parent::M3" << endl;
        M4();
    }
    virtual void M4()
    {
        cout << "Parent::M4" << endl;
   }
};

class Child : public Parent
{
public:
    void M1()
    {
        cout << "Child::M1" << endl;
    }
    virtual void M2()
    {
        cout << "Child::M2" << endl;
    }
    virtual void M4()
    {
        cout << "Child::M4" << endl;
    }
};

void Caller1( Parent *p )
{
    p->M1();
    p->M2();
}

void Caller2( Parent *p )
{
    p->M3();
}

void TestVirtual()
{
    Parent p;
    Caller1( &p ); // Parent::M1\nParent::M2
    Child c;
    Caller1( &c ); // Parent::M1\nChild::M2

    Caller2( &p );
    Caller2( &c );
}

int main() {
    TestVirtual();
}