// Сделать иерархию на подобии: Figure, Square, Circle. Летает и отскакивают от стенок. Объекты должны быть в динамической памяти

#include <iostream>
#include <Windows.h>
#include "AllegroBase.hpp"
using namespace std;

const int FPS = 60;
const int SCREEN_W = 640;
const int SCREEN_H = 480;

class Figure {
	protected:
		int x_;
		int y_;
		int dx_;
		int dy_;
	public:
		Figure()
		{
			Reset();
		}

		void Reset()
		{
			x_ = ( rand() % ( SCREEN_W - 100 ) ) + 50;
			y_ = ( rand() % ( SCREEN_H - 100 ) ) + 50;
			dx_ = 6.0 - rand() % 13;
			dy_ = 6.0 - rand() % 13;
		}

		virtual void Draw()
		{}

		void Collision( double length )
		{
			if ( ( x_ - length <= 0.0) || ( x_ + length >= SCREEN_W ) )
            {
                dx_ = -dx_;
                x_ += dx_;
            }
            if ( ( y_ - length <= 0.0) || ( y_ + length >= SCREEN_H ) )
            {
                dy_ = -dy_;
                y_ += dy_;
            }
		}

		virtual void Move()
		{
			x_ += dx_;
			y_ += dy_;
			
			Collision( 0 );
		}
};
typedef Figure * PFigure;

class Square : public Figure
{
	protected:
		double side_;
	public:
		Square( double side ) :
			Figure(),
			side_( side )
		{
		}

		virtual void Draw()
		{
			double half = side_ / 2;
			al_draw_rectangle ( x_ - half, y_ - half, x_ + half, y_ + half, al_map_rgb( 255, 0, 0 ), 2); //RED
		}

		virtual void Move()
		{
			x_ += dx_;
            y_ += dy_;

            Collision( side_ / 2.0 ); //half
		}
};

class Circle : public Figure
{
	protected:
		double r_;
	public:
		Circle( double r ) :
			Figure(),
			r_( r )
		{
		}

		virtual void Draw()
		{
			al_draw_filled_circle( x_, y_, r_, al_map_rgb( 0, 255, 0) ); //GREEN
		}

		virtual void Move()
		{
			x_ += dx_;
			y_ += dy_;

			Collision( r_ );
		}
};

const int MAX = 10;

class AllegroApp : public AllegroBase
{
    private:
		PFigure figures[MAX];
		int size_;

    public:
        AllegroApp() :
            AllegroBase(),
            size_( 0 )
        {
			memset(	figures, 0, sizeof( figures ) );
        }
		~AllegroApp()
		{
			for( int i = 0; i < size_; i++ )
			{
				delete figures[i];
				figures[i] = 0;
			}
		}

        virtual void Fps()
        {
            for( int i = 0; i < size_; i++ )
			{
				figures[i]->Move();
			}
        }

        virtual void Draw()
        {
            al_clear_to_color( al_map_rgb( 0, 0, 0 ) );
            for( int i = 0; i < size_; i++ )
			{
				figures[i]->Draw();
			}
        }

		virtual void Add( Figure *f )
		{
			if ( size_ >= MAX )
			{
				return;
			}
			figures[ size_ ] = f;
			++size_;
		}

        virtual void OnKeyDown( const ALLEGRO_KEYBOARD_EVENT &keyboard )
        {
            switch ( keyboard.keycode )
            {
                case ALLEGRO_KEY_ENTER:
                    for( int i = 0; i < size_; i++ )
					{
						figures[i]->Reset();
					}
                    break;
                case ALLEGRO_KEY_ESCAPE:
                    Exit();
                    break;
            }

        }
};

int main(int argc, char **argv)
{
    srand( time(0) );

    AllegroApp app;
    if ( !app.Init( SCREEN_W, SCREEN_H, FPS ) )
    {
        return 1;
    }
	
	for( int i = 0; i < MAX; ++i )
	{
		app.Add( new Circle( 10.0 + rand() % 20 ));
	}
    app.Run();

    return 0;
}