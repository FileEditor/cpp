// Выделить список в отдельный класс (FigureList, SreenSaver...)

// Добавить сталкивание( можно по простому, сравнить расстояние от центров. Для круга - это радиус, для квадрата - это половина стороны )
// ПОМЕТКА:
// Работает только для квадрата (столкновения не оптимизированы, но сделаны как можно правдоподобнее)
// Желательно, передалать

#include <iostream>
#include <Windows.h>
#include "AllegroBase.hpp"
#include <cmath>
using namespace std;

const int FPS = 2;
const int SCREEN_W = 640;
const int SCREEN_H = 480;

const int MAX = 6; //Max count of objects(figures)

class Figure {
	protected:
		int x_;
		int y_;
		int dx_;
		int dy_;
	public:
		Figure()
		{
			Reset();
		}

		void Reset()
		{
			x_ = ( rand() % ( SCREEN_W - 100 ) ) + 50;
			y_ = ( rand() % ( SCREEN_H - 100 ) ) + 50;
			dx_ = ((rand() % 2) * 2 - 1) * (rand() % 6 + 1); // 0 / 1 -> -1 / 1 -> -6<>-1 / 1<>6  (can't be 0)
			dy_ = ((rand() % 2) * 2 - 1) * (rand() % 6 + 1);
		}

		virtual void Draw()
		{}

		void WallCollision( float length )
		{
			if ( ( x_ - length <= 0.0) || ( x_ + length >= SCREEN_W ) )
            {
                Collision( true );
            }
            if ( ( y_ - length <= 0.0) || ( y_ + length >= SCREEN_H ) )
            {
                Collision( false );
            }
		}

		void Collision( boolean x )
		{
			if ( x ) // inverse dx
			{
				dx_ = -dx_;
                x_ += dx_;
			}
			else // inverse dy
			{
				dy_ = -dy_;
                y_ += dy_;
			}
		}

		virtual void Move()
		{
			x_ += dx_;
			y_ += dy_;
			
			WallCollision( 0 );
		}

		virtual int GetX()
		{
			return x_;
		}

		virtual int GetY()
		{
			return y_;
		}

		virtual float GetDX()
		{
			return dx_;
		}

		virtual float GetDY()
		{
			return dy_;
		}

		virtual float GetHalf(){ return 0; } // 2xRadius or 1xside
};
typedef Figure * PFigure;

class Square : public Figure
{
	protected:
		float side_;
	public:
		Square( float side ) :
			Figure(),
			side_( side )
		{
		}

		virtual void Draw()
		{
			float half = GetHalf();
			al_draw_rectangle( x_ - half + 1, y_ - half + 1, x_ + half - 1, y_ + half - 1, al_map_rgb( 255, 0, 0 ), 2);
			//RED ; -+1 centers stroke for better alligment https://www.allegro.cc/forums/thread/606662
		}

		virtual void Move()
		{
			x_ += dx_;
            y_ += dy_;

            WallCollision( GetHalf() ); //half
		}

		virtual float GetHalf()
		{
			float half = side_ / 2;
			return half;
		}
};

class Circle : public Figure
{
	protected:
		float r_;
	public:
		Circle( float r ) :
			Figure(),
			r_( r )
		{
		}

		virtual void Draw()
		{
			al_draw_filled_circle( x_, y_, r_, al_map_rgb( 0, 255, 0) ); //GREEN
		}

		virtual void Move()
		{
			x_ += dx_;
			y_ += dy_;

			WallCollision( r_ );
		}

		virtual float GetHalf()
		{
			float half = r_;
			return half;
		}
};

class ScreenSaver
{
	private:
		PFigure* FigureList = new PFigure[MAX];
		int size_;

		boolean Inside( int a, int b, float h1, float h2 )
		{
			return ((a + h1 >= b - h2) && (a + h1 <= b + h2)) || ((a - h1 <= b + h2) && (a - h1 >= b - h2));
		}
	public:
		ScreenSaver() :
			size_( 0 )
		{
			memset( FigureList, 0, sizeof( FigureList ) );
		}
		~ScreenSaver()
		{
			for ( int i = 0; i < size_; i++ )
			{
				delete FigureList[i];
			}
			delete [] FigureList;
		}

		virtual void Add( Figure *f )
		{
			if ( size_ >= MAX )
			{
				return;
			}
			FigureList[size_] = f;
			++size_;
		}
		
		virtual void Next()
        {
            for ( int i = 0; i < size_; i++ )
			{
				FigureList[i]->Move();
			}
			//Check for inter collision
			//ONLY for squares!!!
			for ( int i = 0; i < size_; i++ )
			{
				for ( int j = i+1; j < size_; j++ )
				{
					int x1 = FigureList[i]->GetX();
					int y1 = FigureList[i]->GetY();
					float half1 = FigureList[i]->GetHalf();
					int x2 = FigureList[j]->GetX();
					int y2 = FigureList[j]->GetY();
					float half2 = FigureList[j]->GetHalf();

					if ( Inside( x1, x2, half1, half2 ) && Inside( y1, y2, half1, half2 ) )
					{
						cout << "IN " << i << " | " << j << endl;
						float dx1 = FigureList[i]->GetDX();
						float dy1 = FigureList[i]->GetDY();
						float dx2 = FigureList[j]->GetDX();
						float dy2 = FigureList[j]->GetDY();

						if ( x1 > x2 ) // square 2 to the left from square 1
						{
							if ( y1 > y2 ) // square 2 above square 1
							{
								if ( abs(x2-x1) > abs(y2-y1) ) // square 2 touch left side of square 1 (x movement)
								{
									if ( (dx1 >= 0 && dx2 <= 0) || (dx1 <= 0 && dx2 >= 0) ) // if they have opposite x movement
									{
										FigureList[i]->Collision( true ); // 1: dx = -dx
										FigureList[j]->Collision( true ); // 2: dx = -dx
									}
									else if ( dx1 > 0 && dx2 > 0 ) // if movement is positive (to the right)
									{
										// only possible that square 2 is faster than square 1 in that case.
										// 1: dx = dx , continue, but possible to add some movement
										FigureList[j]->Collision( true ); // 2: dx = -dx
									}
									else if ( dx1 < 0 && dx2 < 0 ) // if movement is negative (to the left)
									{
										// only possible that square 1 is faster than square 2 in that case.
										FigureList[i]->Collision( true ); // 1: dx = -dx
										// 2: dx = dx , continue, but possible to add some movement
									}
								}
								else if ( abs(x2-x1) < abs(y2-y1) ) // square 2 touch upper side of square 1 (y movement)
								{
									if ( (dy1 >= 0 && dy2 <= 0) || (dy1 <= 0 && dy2 >= 0) ) // if they have opposite y movement
									{
										FigureList[i]->Collision( false ); // 1: dy = -dy
										FigureList[j]->Collision( false ); // 2: dy = -dy
									}
									else if ( dy1 > 0 && dy2 > 0 ) // if movement is positive (down)
									{
										// only possible that square 2 is faster than square 1 in that case.
										// 1: dy = dy , continue, but possible to add some movement
										FigureList[j]->Collision( false ); // 2: dy = -dy
									}
									else if ( dy1 < 0 && dy2 < 0 ) // if movement is negative (up)
									{
										// only possible that square 1 is faster than square 2 in that case.
										FigureList[i]->Collision( false ); // 1: dy = -dy
										// 2: dy = dy , continue, but possible to add some movement
									}
								}
								else // right in the corner!
								{
									FigureList[i]->Collision( true ); // 1: dx = -dx
									FigureList[j]->Collision( true ); // 2: dx = -dx
									FigureList[i]->Collision( false ); // 1: dy = -dy
									FigureList[j]->Collision( false ); // 2: dy = -dy
								}
							}
							else if ( y1 < y2 ) // square 2 below square 1
							{
								if ( abs(x2-x1) > abs(y2-y1) ) // square 2 touch left side of square 1 (x movement)
								{
									if ( (dx1 >= 0 && dx2 <= 0) || (dx1 <= 0 && dx2 >= 0) ) // if they have opposite x movement
									{
										FigureList[i]->Collision( true ); // 1: dx = -dx
										FigureList[j]->Collision( true ); // 2: dx = -dx
									}
									else if ( dx1 > 0 && dx2 > 0 ) // if movement is positive (to the right)
									{
										// only possible that square 2 is faster than square 1 in that case.
										// 1: dx = dx , continue, but possible to add some movement
										FigureList[j]->Collision( true ); // 2: dx = -dx
									}
									else if ( dx1 < 0 && dx2 < 0 ) // if movement is negative (to the left)
									{
										// only possible that square 1 is faster than square 2 in that case.
										FigureList[i]->Collision( true ); // 1: dx = -dx
										// 2: dx = dx , continue, but possible to add some movement
									}
								}
								else if ( abs(x2-x1) < abs(y2-y1) ) // square 2 touch lower side of square 1 (y movement)
								{
									if ( (dy1 >= 0 && dy2 <= 0) || (dy1 <= 0 && dy2 >= 0) ) // if they have opposite y movement
									{
										FigureList[i]->Collision( false ); // 1: dy = -dy
										FigureList[j]->Collision( false ); // 2: dy = -dy
									}
									else if ( dy1 > 0 && dy2 > 0 ) // if movement is positive (down)
									{
										// only possible that square 1 is faster than square 2 in that case.
										FigureList[i]->Collision( false ); // 1: dy = -dy
										// 2: dy = dy , continue, but possible to add some movement
									}
									else if ( dy1 < 0 && dy2 < 0 ) // if movement is negative (up)
									{
										// only possible that square 2 is faster than square 1 in that case.
										// 1: dy = dy , continue, but possible to add some movement
										FigureList[j]->Collision( false ); // 2: dy = -dy
									}
								}
								else // right in the corner!
								{
									FigureList[i]->Collision( true ); // 1: dx = -dx
									FigureList[j]->Collision( true ); // 2: dx = -dx
									FigureList[i]->Collision( false ); // 1: dy = -dy
									FigureList[j]->Collision( false ); // 2: dy = -dy
								}
							}
							else // equal y
							{
								if ( (dx1 >= 0 && dx2 <= 0) || (dx1 <= 0 && dx2 >= 0) ) // if they have opposite x movement
								{
									FigureList[i]->Collision( true ); // 1: dx = -dx
									FigureList[j]->Collision( true ); // 2: dx = -dx
								}
								else if ( dx1 > 0 && dx2 > 0 ) // if movement is positive (to the right)
								{
									// only possible that square 2 is faster than square 1 in that case.
									// 1: dx = dx , continue, but possible to add some movement
									FigureList[j]->Collision( true ); // 2: dx = -dx
								}
								else if ( dx1 < 0 && dx2 < 0 ) // if movement is negative (to the left)
								{
									// only possible that square 1 is faster than square 2 in that case.
									FigureList[i]->Collision( true ); // 1: dx = -dx
									// 2: dx = dx , continue, but possible to add some movement
								}
							}
						}


						else if ( x1 < x2 ) // square 2 to the right from square 1
						{
							if ( y1 > y2 ) // square 2 above square 1
							{
								if ( abs(x2-x1) > abs(y2-y1) ) // square 2 touch right side of square 1 (x movement)
								{
										if ( (dx1 >= 0 && dx2 <= 0) || (dx1 <= 0 && dx2 >= 0) ) // if they have opposite x movement
									{
										FigureList[i]->Collision( true ); // 1: dx = -dx
										FigureList[j]->Collision( true ); // 2: dx = -dx
									}
									else if ( dx1 > 0 && dx2 > 0 ) // if movement is positive (to the right)
									{
										// only possible that square 1 is faster than square 2 in that case.
										FigureList[i]->Collision( true ); // 1: dx = -dx
										// 2: dx = dx , continue, but possible to add some movement
									}
									else if ( dx1 < 0 && dx2 < 0 ) // if movement is negative (to the left)
									{
										// only possible that square 2 is faster than square 1 in that case.
										// 1: dx = dx , continue, but possible to add some movement
										FigureList[j]->Collision( true ); // 2: dx = -dx
									}
								}
								else if ( abs(x2-x1) < abs(y2-y1) ) // square 2 touch upper side of square 1 (y movement)
								{
									if ( (dy1 >= 0 && dy2 <= 0) || (dy1 <= 0 && dy2 >= 0) ) // if they have opposite y movement
									{
										FigureList[i]->Collision( false ); // 1: dy = -dy
										FigureList[j]->Collision( false ); // 2: dy = -dy
									}
									else if ( dy1 > 0 && dy2 > 0 ) // if movement is positive (down)
									{
										// only possible that square 2 is faster than square 1 in that case.
										// 1: dy = dy , continue, but possible to add some movement
										FigureList[j]->Collision( false ); // 2: dy = -dy
									}
									else if ( dy1 < 0 && dy2 < 0 ) // if movement is negative (up)
									{
										// only possible that square 1 is faster than square 2 in that case.
										FigureList[i]->Collision( false ); // 1: dy = -dy
										// 2: dy = dy , continue, but possible to add some movement
									}
								}
								else // right in the corner!
								{
									FigureList[i]->Collision( true ); // 1: dx = -dx
									FigureList[j]->Collision( true ); // 2: dx = -dx
									FigureList[i]->Collision( false ); // 1: dy = -dy
									FigureList[j]->Collision( false ); // 2: dy = -dy
								}
							}
							else if ( y1 < y2 ) // square 2 below square 1
							{
								if ( abs(x2-x1) > abs(y2-y1) ) // square 2 touch right side of square 1 (x movement)
								{
									if ( (dx1 >= 0 && dx2 <= 0) || (dx1 <= 0 && dx2 >= 0) ) // if they have opposite x movement
									{
										FigureList[i]->Collision( true ); // 1: dx = -dx
										FigureList[j]->Collision( true ); // 2: dx = -dx
									}
									else if ( dx1 > 0 && dx2 > 0 ) // if movement is positive (to the right)
									{
										// only possible that square 2 is faster than square 1 in that case.
										// 1: dx = dx , continue, but possible to add some movement
										FigureList[j]->Collision( true ); // 2: dx = -dx
									}
									else if ( dx1 < 0 && dx2 < 0 ) // if movement is negative (to the left)
									{
										// only possible that square 1 is faster than square 2 in that case.
										FigureList[i]->Collision( true ); // 1: dx = -dx
										// 2: dx = dx , continue, but possible to add some movement
									}
								}
								else if ( abs(x2-x1) < abs(y2-y1) ) // square 2 touch lower side of square 1 (y movement)
								{
									if ( (dy1 >= 0 && dy2 <= 0) || (dy1 <= 0 && dy2 >= 0) ) // if they have opposite y movement
									{
										FigureList[i]->Collision( false ); // 1: dy = -dy
										FigureList[j]->Collision( false ); // 2: dy = -dy
									}
									else if ( dy1 > 0 && dy2 > 0 ) // if movement is positive (down)
									{
										// only possible that square 1 is faster than square 2 in that case.
										FigureList[i]->Collision( false ); // 1: dy = -dy
										// 2: dy = dy , continue, but possible to add some movement
									}
									else if ( dy1 < 0 && dy2 < 0 ) // if movement is negative (up)
									{
										// only possible that square 2 is faster than square 1 in that case.
										// 1: dy = dy , continue, but possible to add some movement
										FigureList[j]->Collision( false ); // 2: dy = -dy
									}
								}
								else // right in the corner!
								{
									FigureList[i]->Collision( true ); // 1: dx = -dx
									FigureList[j]->Collision( true ); // 2: dx = -dx
									FigureList[i]->Collision( false ); // 1: dy = -dy
									FigureList[j]->Collision( false ); // 2: dy = -dy
								}
							}
							else // equal y
							{
								if ( (dx1 >= 0 && dx2 <= 0) || (dx1 <= 0 && dx2 >= 0) ) // if they have opposite x movement
								{
									FigureList[i]->Collision( true ); // 1: dx = -dx
									FigureList[j]->Collision( true ); // 2: dx = -dx
								}
								else if ( dx1 > 0 && dx2 > 0 ) // if movement is positive (to the right)
								{
									// only possible that square 1 is faster than square 2 in that case.
									FigureList[i]->Collision( true ); // 1: dx = -dx
									// 2: dx = dx , continue, but possible to add some movement
								}
								else if ( dx1 < 0 && dx2 < 0 ) // if movement is negative (to the left)
								{
									// only possible that square 2 is faster than square 1 in that case.
									// 1: dx = dx , continue, but possible to add some movement
									FigureList[j]->Collision( true ); // 2: dx = -dx
								}
							}
						}
						
						
						else // equal x
						{
							if ( (dy1 >= 0 && dy2 <= 0) || (dy1 <= 0 && dy2 >= 0) ) // if they have opposite y movement
							{
								FigureList[i]->Collision( false ); // 1: dy = -dy
								FigureList[j]->Collision( false ); // 2: dy = -dy
							}
							else if ( y1 > y2 ) // square 2 above square 1
							{
								if ( dy1 > 0 && dy2 > 0 ) // if movement is positive (down)
								{
									// only possible that square 2 is faster than square 1 in that case.
									// 1: dy = dy , continue, but possible to add some movement
									FigureList[j]->Collision( false ); // 2: dy = -dy
								}
								else if ( dx1 < 0 && dx2 < 0 ) // if movement is negative (to the left)
								{
									// only possible that square 1 is faster than square 2 in that case.
									FigureList[i]->Collision( false ); // 1: dy = -dy
									// 2: dy = dy , continue, but possible to add some movement
								}
							}
							else if ( y1 < y2 ) // square 2 below square 1
							{
								if ( dy1 > 0 && dy2 > 0 ) // if movement is positive (down)
								{
									// only possible that square 1 is faster than square 2 in that case.
									FigureList[i]->Collision( false ); // 1: dy = -dy
									// 2: dy = dy , continue, but possible to add some movement
								}
								else if ( dx1 < 0 && dx2 < 0 ) // if movement is negative (to the left)
								{
									// only possible that square 2 is faster than square 1 in that case.
									// 1: dy = dy , continue, but possible to add some movement
									FigureList[j]->Collision( false ); // 2: dy = -dy
								}
							}
						}
					}
				
				
				}
			}
        }

		virtual void Draw(ALLEGRO_FONT *font)
		{
			al_clear_to_color( al_map_rgb( 0, 0, 0 ) );
			for ( int i = 0; i < size_; i++ )
			{
				FigureList[i]->Draw();
				string s = to_string(i);
				al_draw_text(font, al_map_rgb(255, 0, 0), FigureList[i]->GetX(), FigureList[i]->GetY(), 0, s.c_str());
			}
		}

		virtual void Reset()
		{
			for ( int i = 0; i < size_; i++ )
			{
				FigureList[i]->Reset();
			}
		}

};

class AllegroApp : public AllegroBase
{
    private:
		ScreenSaver ss;
	public:
        AllegroApp() :
            AllegroBase()
        {
			for ( int i = 0; i < MAX; ++i )
			{
				//CHANGE ME 
				ss.Add( new Square( 20.0 + rand() % 41 ) ); //Circle( 10.0 + rand() % 20 )
			}
        }
		~AllegroApp()
		{
		}

		virtual void Fps()
		{
			ss.Next();
		}

		virtual void Draw()
		{
			ss.Draw(alfont_);
		}

        virtual void OnKeyDown( const ALLEGRO_KEYBOARD_EVENT &keyboard )
        {
            switch ( keyboard.keycode )
            {
                case ALLEGRO_KEY_ENTER:
                    ss.Reset();
                    break;
                case ALLEGRO_KEY_ESCAPE:
                    Exit();
                    break;
            }

        }
};

int main(int argc, char **argv)
{
    srand( time(0) );

    AllegroApp app;
    if ( !app.Init( SCREEN_W, SCREEN_H, FPS ) )
    {
        return 1;
    }
	
    app.Run();

    return 0;
}