// Переделать проект с прошлого занятия + Интерфейсы

#include "AllegroBase.hpp"
#include <cmath>
#include <cstdlib>

const int MAX = 4; //Max count of objects(figures)

class Figure {
	protected:
		int x_;
		int y_;
	public:
		virtual void Reset()
		{
			x_ = ( rand() % ( SCREEN_W - 100 ) ) + 50;
			y_ = ( rand() % ( SCREEN_H - 100 ) ) + 50;
		}

		virtual void Draw()
		{}

		virtual void Move()
		{}

		virtual int GetX()
		{
			return x_;
		}

		virtual int GetY()
		{
			return y_;
		}
};
typedef Figure * PFigure;

class MoveableFigure : public Figure
{
	protected:
		int dx_;
		int dy_;
	public:
		MoveableFigure()
		{
			Reset();
		}
		
		void Reset()
		{
			Figure::Reset();
			dx_ = ((rand() % 2) * 2 - 1) * (rand() % 6 + 1); // 0 / 1 -> -1 / 1 -> -6<>-1 / 1<>6  (can't be 0)
			dy_ = ((rand() % 2) * 2 - 1) * (rand() % 6 + 1);
		}

		void WallCollision( float length )
		{
			if ( ( x_ - length <= 0.0) || ( x_ + length >= SCREEN_W ) )
			{
				dx_ = -dx_;
				x_ += dx_;
			}
			if ( ( y_ - length <= 0.0) || ( y_ + length >= SCREEN_H ) )
			{
				dy_ = -dy_;
				y_ += dy_;
			}
		}

		void Move()
		{
			x_ += dx_;
			y_ += dy_;
			
			WallCollision( 0 );
		}

		virtual float GetDX()
		{
			return dx_;
		}

		virtual float GetDY()
		{
			return dy_;
		}

		float GetHalf(){ return 0; } //2xRadius or 1xSide
};
typedef MoveableFigure * PMoveableFigure;

class MoveableSquare : public MoveableFigure
{
	protected:
		float side_;
	public:
		MoveableSquare( float side ) :
			MoveableFigure(),
			side_( side )
		{
		}

		virtual void Draw()
		{
			float half = GetHalf();
			al_draw_rectangle( x_ - half + 1, y_ - half + 1, x_ + half - 1, y_ + half - 1, al_map_rgb( 255, 0, 0 ), 2);
			//RED ; -+1 centers stroke for better alligment https://www.allegro.cc/forums/thread/606662
		}

		virtual void Move()
		{
			x_ += dx_;
			y_ += dy_;

			WallCollision( GetHalf() ); //half
		}

		virtual float GetHalf()
		{
			float half = side_ / 2;
			return half;
		}
};

class MoveableCircle : public MoveableFigure
{
	protected:
		float r_;
	public:
		MoveableCircle( float r ) :
			MoveableFigure(),
			r_( r )
		{
		}

		virtual void Draw()
		{
			al_draw_circle( x_, y_, r_, al_map_rgb( 255, 0, 0), 2 ); //RED
		}

		virtual void Move()
		{
			x_ += dx_;
			y_ += dy_;

			WallCollision( GetHalf() );
		}

		virtual float GetHalf()
		{
			float half = r_;
			return half;
		}
};


class SizeableFigure : public Figure
{
	protected:
		float maxk; // max koeficent for sizing; min is 1
		float k_;	// current k (from 1 to maxk)
		float dk_;	// current k step amount and direction (inside/outside)
	private:
		void Limit()
		{
			if ((k_ < 1) || (k_ >= maxk))
			{
				dk_ = -dk_;
			}
		}
	public:
		SizeableFigure()
		{
			k_ = 1;
			Reset();
		}

		void Reset()
		{
			Figure::Reset();
			dk_ = (((rand() % 2) * 2 - 1) * (rand() % 8+ 1)) / 3.000; // 0/1 -> 0/2 -> -1/1 -> -2.6<>-0.3 / 0.3<>2.6  (can't be 0)
			maxk = ((rand() % 25 + 1) / 3.000) + 10; // 0<>24 -> 1/3<>25/3 -> 10.3<>18.3
		}

		void WallCollision( float length )
		{
			if ( ( x_ - length <= 0.0) || ( x_ + length >= SCREEN_W ) || ( y_ - length <= 0.0) || ( y_ + length >= SCREEN_H ) )
			{
				dk_ = -dk_;
			}
		}

		virtual void Move()
		{
			Limit();
			k_ += dk_;
		}
};
typedef SizeableFigure * PSizeableFigure;

class SizeableSquare : public SizeableFigure
{
	protected:
		float side_;
		float default_;
	public:
		SizeableSquare( float side ) :
			SizeableFigure(),
			side_( side )
		{
			default_ = side_;
		}

		virtual void Draw()
		{
			float half = GetHalf();
			al_draw_filled_rectangle( x_ - half + 1, y_ - half + 1, x_ + half - 1, y_ + half - 1, al_map_rgb( 0, 255, 0 ));
			//Green ; -+1 centers stroke for better alligment https://www.allegro.cc/forums/thread/606662
		}

		virtual void Move()
		{
			SizeableFigure::Move();
			side_ = default_ + k_;
			WallCollision( GetHalf() );
		}

		virtual float GetHalf()
		{
			float half = side_ / 2;
			return half;
		}
};

class SizeableCircle : public SizeableFigure
{
	protected:
		float r_;
		float default_;
	public:
		SizeableCircle( float r ) :
			SizeableFigure(),
			r_( r )
		{
			default_ = r_;
		}

		virtual void Draw()
		{
			al_draw_filled_circle( x_, y_, r_, al_map_rgb( 0, 255, 0)); //GREEN
		}

		virtual void Move()
		{
			SizeableFigure::Move();
			r_ = default_ + k_/2.000;
			WallCollision( GetHalf() );
		}

		virtual float GetHalf()
		{
			float half = r_;
			return half;
		}
};

class ScreenSaver
{
	private:
		PFigure* FigureList = new PFigure[MAX];
		int size_;
	public:
		ScreenSaver() :
			size_( 0 )
		{
			memset( FigureList, 0, sizeof( FigureList ) );
		}
		~ScreenSaver()
		{
			for ( int i = 0; i < size_; i++ )
			{
				delete FigureList[i];
			}
			delete [] FigureList;
		}

		virtual void Add( Figure *f )
		{
			if ( size_ >= MAX )
			{
				return;
			}
			FigureList[size_] = f;
			++size_;
		}
		
		virtual void Next()
		{
			for ( int i = 0; i < size_; i++ )
			{
				FigureList[i]->Move();
			}
		}

		virtual void Draw(ALLEGRO_FONT *font)
		{
			al_clear_to_color( al_map_rgb( 0, 0, 0 ) );
			for ( int i = 0; i < size_; i++ )
			{
				FigureList[i]->Draw();
				/* string s = to_string(i);
				al_draw_text(font, al_map_rgb(255, 255, 255), FigureList[i]->GetX(), FigureList[i]->GetY(), 0, s.c_str()); */
			}
		}

		virtual void Reset()
		{
			for ( int i = 0; i < size_; i++ )
			{
				FigureList[i]->Reset();
			}
		}

};


class AllegroApp : public AllegroBase
{
	private:
		ScreenSaver ss;
	public:
		AllegroApp() :
			AllegroBase()
		{
			for ( int i = 1; i <= MAX; ++i )
			{
				//CHANGE ME 
				switch (i % 4) {
					case 0 :
						ss.Add( new MoveableSquare( 20.0 + rand() % 41 ) );
						break;
					case 1 :
						ss.Add( new MoveableCircle( 10.0 + rand() % 21 ) );
						break;
					case 2 :
						ss.Add( new SizeableSquare( 10.0 + rand() % 41 ) );
						break;
					case 3 :
						ss.Add( new SizeableCircle( 20.0 + rand() % 21 ) );
						break;
					default:
						break;
				}
				
			}
		}
		~AllegroApp()
		{
		}

		virtual void Fps()
		{
			ss.Next();
		}

		virtual void Draw()
		{
			ss.Draw(alfont_);
		}

		virtual void OnKeyDown( const ALLEGRO_KEYBOARD_EVENT &keyboard )
		{
			switch ( keyboard.keycode )
			{
				case ALLEGRO_KEY_ENTER:
					ss.Reset();
					break;
				case ALLEGRO_KEY_ESCAPE:
					Exit();
					break;
			}

		}
};

int main(int argc, char **argv)
{
	srand( time(0) );

	AllegroApp app;
	if ( !app.Init( SCREEN_W, SCREEN_H, FPS ) )
	{
		return 1;
	}
	
	app.Run();

	return 0;
}